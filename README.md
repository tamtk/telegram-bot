# Telegram-bot

This project is guild how create Telegram-bot for your personal task.

## Requirements

* Git
* Modules listed in requirements.txt

## Preparing

* The best practice is working with python enviroment
`virtualenv -p ` which python3` venv`
Please note that the usage of a virtualenv is recommended:
`source venv/bin/activate`
* You will need to install the requirements to use it
`pip install -r requirements.txt`

## Principles

There is two actions should be done:
* Create Telegram-bot
* Define action/task/script ... for Telegram-bot

## How to create Telegram-bot
* Install and regist your account on Telegram
  * Download Telegram app on your mobile phone
  * Register new account if you don't have
* Create newbot with BotFather
  * In the search box, type `BotFather`, then hit enter
  * Start conversation with `BotFater`, then send message `/newbot`
  * Type the name of Bot
  * Type username for Bot
  * After that, `BotFather` will reply the token for your Bot example: `1125279755:AAEye4gpaatRtcteoadkjziyW6Db1KRvvlsO8I`

## Create source code for New Bot

* Create script for your bot: `main.py`

```
from telegram.ext import Updater, CommandHandler
import requests
from datetime import date

message_help = """
Hello User,

I'm a bot for you.
Here is a list of action that I can do for you.
/help   => Show list of task I can do
/botstatus  => This is my status
/botrestart => Restart myself
/time   => Show current time
/whoami => Who am i
"""

def whoami(bot, update):
    message = """Really? You don't know who am i ^_^, just kidding, I'm a bot you and I can help you a lot"""
    chat_id = update.message.chat_id
    bot.send_message(chat_id=chat_id, text=message)


def time(bot, update):
    today = date.today()
    message = today.strftime("%B %d, %Y")
    chat_id = update.message.chat_id
    bot.send_message(chat_id=chat_id, text=message)


def botstatus(bot, update):
    message = "Today I'm feel so good"
    chat_id = update.message.chat_id
    bot.send_message(chat_id=chat_id, text=message)


def help(bot, update):
    chat_id = update.message.chat_id
    is_admin = isadmin(bot, update)
    if is_admin:
        bot.send_message(chat_id=chat_id, text=message_help)
    else:
        bot.send_message(chat_id=chat_id, text=message_not_permission)


def main():
    updater = Updater("1125279755:AAEye4gpaatRtcteoadkjziyW6Db1KRvvlsO8I")
    dp = updater.dispatcher
    dp.add_handler(CommandHandler("whoami", whoami))
    dp.add_handler(CommandHandler("time", time))
    dp.add_handler(CommandHandler("botstatus", botstatus))
    dp.add_handler(CommandHandler("help", help))
    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    main()

```

```bash
Remember that we must change the bot Token in first step.
```

Now, Start your bot and enjoy it.

```
python bin/main.py
```

## Documentation

The documentation of the code is available at:
`https://tamtk.gitlab.io/telegram-bot/`


## Usage

```
usage: main.py [-h help]   
               [-v verbose]

The main used case is by calling directly the module, example:

`python main.py`
