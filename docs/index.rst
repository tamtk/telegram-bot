.. Telegram-bot documentation master file, created by
   sphinx-quickstart on Mon May 18 02:17:05 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. mdinclude:: ../README.md

.. toctree::
   :hidden:
   :maxdepth: 2

   index
   lib

