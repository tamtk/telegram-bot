from telegram.ext import Updater, CommandHandler
from datetime import date

message_help = """
Hello User,

I'm a bot for you.
Here is a list of action that I can do for you.
/help   => Show list of task I can do
/botstatus  => This is my status
/botrestart => Restart myself
/time   => Show current time
/whoami => Who am i
"""


def whoami(bot, update):
    """
    This function will response message define before.
    """
    message = """
Really? You don't know who am i ^_^,
just kidding, I'm a bot you and I can help you a lot
"""
    chat_id = update.message.chat_id
    bot.send_message(chat_id=chat_id, text=message)


def time(bot, update):
    """
    This function will be response current time of bot.
    """
    today = date.today()
    message = today.strftime("%B %d, %Y")
    chat_id = update.message.chat_id
    bot.send_message(chat_id=chat_id, text=message)


def botstatus(bot, update):
    """
    This func will response message of bot_status

    """
    message = "Today I'm feel so good"
    chat_id = update.message.chat_id
    bot.send_message(chat_id=chat_id, text=message)


def help(bot, update):
    """
    This func will response attribute of Bot
    """
    chat_id = update.message.chat_id
    bot.send_message(chat_id=chat_id, text=message_help)


def main():
    """
    This is main bot.

    The script must provide the API Token.

    """
    updater = Updater("1125279755:AAEye4gpaatRtcteoadkjziyW6Db1KRvvlsO8I")
    dp = updater.dispatcher
    dp.add_handler(CommandHandler("whoami", whoami))
    dp.add_handler(CommandHandler("time", time))
    dp.add_handler(CommandHandler("botstatus", botstatus))
    dp.add_handler(CommandHandler("help", help))
    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    main()
